package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.IRentService;
import ro.ubb.catalog.web.converter.RentConverter;
import ro.ubb.catalog.web.dto.RentDto;
import ro.ubb.catalog.web.dto.RentsDto;


@RestController
public class RentController {

    public static final Logger log = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private IRentService rentService;

    @Autowired
    private RentConverter rentConverter;

    @RequestMapping(value = "/rents", method = RequestMethod.GET)
    RentsDto getRents() {
        log.trace("getRents(RentController) --- method entered");
        RentsDto result = new RentsDto(rentConverter
                .convertModelsToDtos(rentService.getAllRents()));
        log.trace("getRents(RentController): result={}", result);
        return result;
    }

    @RequestMapping(value = "/rents", method = RequestMethod.POST)
    RentDto saveMovie(@RequestBody RentDto rentDto) {
        log.trace("saveRent(RentController) - method entered: rent={}", rentConverter.convertDtoToModel(rentDto));
        RentDto result = rentConverter.convertModelToDto(rentService.saveRent(rentConverter.convertDtoToModel(rentDto)));
        log.trace("saveRent(RentController) - method finished");
        return result;
    }


    @RequestMapping(value = "/rents/{id}", method = RequestMethod.PUT)
    RentDto updateRent(@PathVariable Long id,
                       @RequestBody RentDto rentDto) {
        log.trace("updateRent(RentController) - method entered: rent={}", rentConverter.convertDtoToModel(rentDto));
        RentDto result = rentConverter.convertModelToDto(rentService.updateRent(id,
                rentConverter.convertDtoToModel(rentDto)));
        log.trace("updateRent(RentController) - method finished");
        return result;

    }


    @RequestMapping(value = "/rents/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteRent(@PathVariable Long id) {
        log.trace("deleteRent(RentController) - method entered: id={}", id);
        rentService.deleteById(id);
        log.trace("deleteRent(RentController) - method finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
