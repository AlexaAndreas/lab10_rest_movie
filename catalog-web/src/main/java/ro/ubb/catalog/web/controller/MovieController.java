package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.IMovieService;
import ro.ubb.catalog.web.converter.MovieConverter;
import ro.ubb.catalog.web.dto.MovieDto;
import ro.ubb.catalog.web.dto.MoviesDto;

@RestController
public class MovieController {
    public static final Logger log = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private IMovieService movieService;

    @Autowired
    private MovieConverter movieConverter;

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    MoviesDto getMovies() {
        log.trace("getMovies(MovieController) --- method entered");
        MoviesDto result = new MoviesDto(movieConverter
                .convertModelsToDtos(movieService.getAllMovies()));
        log.trace("getMovies(MovieService): result={}", result);
        return result;
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
    MovieDto saveMovie(@RequestBody MovieDto movieDto) {
        log.trace("saveMovie(MovieController) - method entered: movie={}", movieConverter.convertDtoToModel(movieDto));
        MovieDto result = movieConverter.convertModelToDto(
                movieService.saveMovie(
                        movieConverter.convertDtoToModel(movieDto)));
        log.trace("saveMovie(MovieController) - method finished");
        return  result;
    }

    @RequestMapping(value = "/movies/{id}", method = RequestMethod.PUT)
    MovieDto updateMovie(@PathVariable Long id,
                             @RequestBody MovieDto movieDto) {
        log.trace("updateMovie(MovieController) - method entered: movie={}", movieConverter.convertDtoToModel(movieDto));
        MovieDto result = movieConverter.convertModelToDto( movieService.updateMovie(id,
                movieConverter.convertDtoToModel(movieDto)));
        log.trace("updateMovie(MovieController) - method finished");
        return  result;
    }


    @RequestMapping(value = "/movies/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteMovie(@PathVariable Long id){
        log.trace("deleteMovie(MovieController) - method entered: id={}", id);
        movieService.deleteById(id);
        log.trace("deleteMovie(MovieController) - method finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
