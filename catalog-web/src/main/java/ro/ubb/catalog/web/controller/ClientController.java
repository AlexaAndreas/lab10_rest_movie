package ro.ubb.catalog.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.ubb.catalog.core.service.IClientService;
import ro.ubb.catalog.web.converter.ClientConverter;
import ro.ubb.catalog.web.dto.ClientDto;
import ro.ubb.catalog.web.dto.ClientsDto;



@RestController
public class ClientController {
    public static final Logger log = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private IClientService clientService;

    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    ClientsDto getClients() {
        log.trace("getClients(ClientController) --- method entered");
        ClientsDto result = new ClientsDto(clientConverter.convertModelsToDtos(clientService.getAllClients()));
        log.trace("getClients(ClientController): result={}", result);
        return result;
    }


    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto clientDto) {
        log.trace("saveClient(ClientController) - method entered: client={}", clientConverter.convertDtoToModel(clientDto));
        ClientDto result =  clientConverter.convertModelToDto(
                clientService.saveClient(
                        clientConverter.convertDtoToModel(clientDto)));
        log.trace("saveClient(ClientController) - method finished");
        return result;
    }


    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto updateMovie(@PathVariable Long id,
                         @RequestBody ClientDto clientDto) {
        log.trace("updateClient(ClientController) - method entered: client={}", clientConverter.convertDtoToModel(clientDto));
        ClientDto result = clientConverter.convertModelToDto( clientService.updateClient(id,
                clientConverter.convertDtoToModel(clientDto)));
        log.trace("updateClient(ClientController) - method finished");
        return result;
    }


    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable Long id){
        log.trace("deleteClient(ClientController) - method entered: id={}",id);
        clientService.deleteById(id);
        log.trace("deleteClient(ClientController) - method finished");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
