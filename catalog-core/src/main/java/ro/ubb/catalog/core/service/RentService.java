package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Rent;
import ro.ubb.catalog.core.repository.RentRepository;


import java.util.List;

@Service
public class RentService implements IRentService{
    public static final Logger log = LoggerFactory.getLogger(RentService.class);
    @Autowired
    private RentRepository rentRepository;
    @Override
    public List<Rent> getAllRents() {
        log.trace("getAllRents(RentService) --- method entered");
        List<Rent> result = rentRepository.findAll();
        log.trace("getAllRents(RentService): result={}", result);
        return result;
    }

    @Override
    public Rent saveRent(Rent rent) {
        log.trace("saveRent(RentService) - method entered: rent={}", rent);
        Rent result = rentRepository.save(rent);
        log.trace("saveRent(RentService) - method finished");
        return result;
    }

    @Override
    @Transactional
    public Rent updateRent(Long id, Rent rent) {
        log.trace("updateRent(RentService) - method entered: rent={}", rent);
        Rent update = rentRepository.findById(id).orElse(rent);
        update.setClientId(rent.getClientId());
        update.setMovieId(rent.getMovieId());
        update.setDueDate(rent.getDueDate());
        log.trace("updateRent(RentService) - method finished");
        return update;
    }

    @Override
    public void deleteById(Long id) {
        log.trace("deleteById(RentService) - method entered: id={}", id);
        rentRepository.deleteById(id);
        log.trace("deleteById(RentService) - method finished");
    }
}
