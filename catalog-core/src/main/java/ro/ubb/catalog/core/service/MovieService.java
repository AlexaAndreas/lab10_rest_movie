package ro.ubb.catalog.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.catalog.core.model.Movie;
import ro.ubb.catalog.core.repository.MovieRepository;


import java.util.List;

@Service
public class MovieService implements IMovieService {
    public static final Logger log = LoggerFactory.getLogger(MovieService.class);
    @Autowired
    private MovieRepository movieRepository;

    @Override
    public List<Movie> getAllMovies() {
        log.trace("getAllMovies(MovieService) --- method entered");
        List<Movie> result = movieRepository.findAll();
        log.trace("getAllMovies(MovieService): result={}", result);
        return result;
    }

    @Override
    public Movie saveMovie(Movie movie) {
        log.trace("saveMovie(MovieService) - method entered: movie={}", movie);
        Movie result = movieRepository.save(movie);
        log.trace("saveMovie(MovieService) - method finished");
        return result;
    }

    @Override
    @Transactional
    public Movie updateMovie(Long id, Movie movie) {
        log.trace("updateMovie(MovieService) - method entered: movie={}", movie);
        Movie update = movieRepository.findById(id).orElse(movie);
        update.setGenre(movie.getGenre());
        update.setName(movie.getName());
        log.trace("updateMovie(MovieService) - method finished");
        return update;
    }

    @Override
    public void deleteById(Long id) {
        log.trace("deleteById(MovieService) - method entered: id={}", id);
        movieRepository.deleteById(id);
        log.trace("deleteById(MovieService) - method finished");
    }
}
