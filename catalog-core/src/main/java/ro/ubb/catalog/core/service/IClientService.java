package ro.ubb.catalog.core.service;



import ro.ubb.catalog.core.model.Client;

import java.util.List;

public interface IClientService {
    List<Client> getAllClients();

    Client saveClient(Client client);

    Client updateClient(Long id, Client client);

    void deleteById(Long id);
}
