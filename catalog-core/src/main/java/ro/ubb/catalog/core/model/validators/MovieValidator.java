package ro.ubb.catalog.core.model.validators;


import ro.ubb.catalog.core.model.Movie;

public class MovieValidator implements Validator<Movie>{
    @Override
    public void validate(Movie entity) throws ValidatorException {
        if (entity.getName().isEmpty()) {
            throw new ValidatorException("Illegal name given");
        }


    }
}
