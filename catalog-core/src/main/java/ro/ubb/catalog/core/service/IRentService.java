package ro.ubb.catalog.core.service;



import ro.ubb.catalog.core.model.Rent;

import java.util.List;

public interface IRentService {
    List<Rent> getAllRents();

    Rent saveRent(Rent rent);

    Rent updateRent(Long id, Rent rent);

    void deleteById(Long id);
}
