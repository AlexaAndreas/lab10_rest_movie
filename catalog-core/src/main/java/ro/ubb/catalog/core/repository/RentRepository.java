package ro.ubb.catalog.core.repository;


import ro.ubb.catalog.core.model.Rent;

public interface RentRepository extends Repository<Rent, Long> {
}
