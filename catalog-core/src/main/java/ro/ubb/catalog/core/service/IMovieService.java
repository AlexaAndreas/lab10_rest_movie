package ro.ubb.catalog.core.service;


import ro.ubb.catalog.core.model.Movie;

import java.util.List;

public interface IMovieService {
    List<Movie> getAllMovies();

    Movie saveMovie(Movie movie);

    Movie updateMovie(Long id, Movie movie);

    void deleteById(Long id);
}
