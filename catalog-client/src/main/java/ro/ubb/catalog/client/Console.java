package ro.ubb.catalog.client;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ro.ubb.catalog.web.dto.*;


import java.util.Scanner;


@Component
public class Console {
    public static final String MOVIE_URL = "http://localhost:8080/api/movies";
    public static final String CLIENT_URL = "http://localhost:8080/api/clients";
    public static final String RENT_URL = "http://localhost:8080/api/rents";
    RestTemplate restTemplate;

    public Console(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public void runConsole() {
        try {

            while (true) {
                mainMenu();
                Scanner in = new Scanner(System.in);
                int input = in.nextInt();
                if (input == 1) {
                    movieMenu();
                    int option = in.nextInt();
                    if (option == 1) {
                        System.out.print("name: ");
                        String name = in.next();
                        System.out.print("genre: ");
                        String genre = in.next();
                        restTemplate.postForObject(
                                MOVIE_URL,
                                new MovieDto(name, genre),
                                MovieDto.class);
                    } else if (option == 2) {
                        System.out.print("id: ");
                        Long id = in.nextLong();
                        System.out.print("new name: ");
                        String name = in.next();
                        System.out.print("new genre: ");
                        String genre = in.next();

                        MoviesDto allMovies = restTemplate.getForObject(MOVIE_URL, MoviesDto.class);
                        for (MovieDto i : allMovies.getMovies())
                            if (i.getId() == id) {
                                i.setName(name);
                                i.setGenre(genre);
                                restTemplate.put(MOVIE_URL + "/{id}", i, i.getId());
                            }
                    } else if (option == 3) {
                        System.out.print("id: ");
                        Long id = in.nextLong();
                        MoviesDto allMovies = restTemplate.getForObject(MOVIE_URL, MoviesDto.class);
                        for (MovieDto i : allMovies.getMovies())
                            if (i.getId() == id) {
                                restTemplate.delete(MOVIE_URL + "/{id}", i.getId());
                            }
                    } else if (option == 4) {
                        MoviesDto allMovies = restTemplate.getForObject(MOVIE_URL, MoviesDto.class);
                        System.out.println(allMovies);
                    } else {
                        System.out.println("Invalid command");
                    }
                } else if (input == 2) {
                    clientMenu();
                    int option = in.nextInt();
                    if (option == 1) {
                        System.out.print("name: ");
                        String name = in.next();
                        System.out.print("cnp: ");
                        String cnp = in.next();
                        restTemplate.postForObject(
                                CLIENT_URL,
                                new ClientDto(name, cnp),
                                ClientDto.class);
                    } else if (option == 2) {
                        System.out.print("id: ");
                        Long id = in.nextLong();
                        System.out.print("new name: ");
                        String name = in.next();
                        System.out.print("new cnp: ");
                        String cnp = in.next();
                        ClientsDto allClients = restTemplate.getForObject(CLIENT_URL, ClientsDto.class);
                        for (ClientDto i : allClients.getClients())
                            if (i.getId() == id) {
                                i.setName(name);
                                i.setCnp(cnp);
                                restTemplate.put(CLIENT_URL + "/{id}", i, i.getId());
                            }
                    } else if (option == 3) {
                        System.out.print("id: ");
                        Long id = in.nextLong();
                        ClientsDto allClients = restTemplate.getForObject(CLIENT_URL, ClientsDto.class);
                        for (ClientDto i : allClients.getClients())
                            if (i.getId() == id) {
                                restTemplate.delete(CLIENT_URL + "/{id}", i.getId());

                            }
                    } else if (option == 4) {
                        ClientsDto allClients = restTemplate.getForObject(CLIENT_URL, ClientsDto.class);
                        System.out.println(allClients);
                    } else {
                        System.out.println("Invalid command");
                    }
                } else if (input == 3) {
                    rentMenu();
                    int option = in.nextInt();
                    if (option == 1) {
                        System.out.print("client id: ");
                        Long cid = in.nextLong();
                        System.out.print("movie id: ");
                        Long mid = in.nextLong();
                        System.out.print("due date: ");
                        String date = in.next();
                        // rentService.saveRent(new Rent(mid, cid, date));
                    } else if (option == 2) {
                        System.out.print("id: ");
                        Long id = in.nextLong();
                        System.out.print("client id: ");
                        Long cid = in.nextLong();
                        System.out.print("movie id: ");
                        Long mid = in.nextLong();
                        System.out.print("due date: ");
                        String date = in.next();
                        //rentService.updateRent(new Rent(id, mid, cid, date));
                    } else if (option == 3) {
                        System.out.print("id: ");
                        Long id = in.nextLong();
                        //rentService.deleteById(id);
                    } else if (option == 4) {
                        RentsDto allRents = restTemplate.getForObject(RENT_URL, RentsDto.class);
                        System.out.println(allRents);
                    } else {
                        System.out.println("Invalid command");
                    }
                } else if (input == 0) {
                    return;
                } else {
                    System.out.println("Invalid input!");
                }

            }

        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }

    private void mainMenu() {
        System.out.println("0.Exit\n1.Manage Movies\n2.Manage Clients\n3.Manage Rentals\n");
    }

    private void movieMenu() {
        System.out.println("1.Add movie\n2.Update Movie\n3.Delete Movie\n4.Show all movies\n");
    }

    private void clientMenu() {
        System.out.println("1.Add client\n2.Update client\n3.Delete client\n4.Show all clients\n");
    }

    private void rentMenu() {
        System.out.println("1.Add rental\n2.Update rental\n3.Delete rental\n4.Show all rentals\n");
    }


}
